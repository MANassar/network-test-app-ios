//
//  ViewController.swift
//  Network Test App
//
//  Created by Nassar, Mohamed on 12.01.20.
//  Copyright © 2020 MNassar. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class ViewController: UIViewController {

    //MARK:- Constants Sa3y
    
    let urlSession = URLSession(configuration: .default)
    
    let sa3yApiUrlString_GET = "https://m3kzetbe3d.execute-api.eu-central-1.amazonaws.com/Live/item"
    let sa3yApiUrlString_POST = "https://m3kzetbe3d.execute-api.eu-central-1.amazonaws.com/Live/cart"
    
    //MARK:- Constants Open Weather
    let apiKey = "744c3d5271daaaca5928744840ebbb65"
    let baseUrlString = "https://api.openweathermap.org/data/2.5/weather"
    
    //MARK:- Variables
    var urlComponents:URLComponents!
    var startTime:Date?
    var endTime:Date?
    
    
    //MARK:- Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        urlComponents = URLComponents(string: baseUrlString)
        let queryItems = [
            URLQueryItem(name: "q", value: "Linz"),
            URLQueryItem(name: "appid", value: apiKey)
        ]

        urlComponents!.queryItems = queryItems
    }

    //MARK:- UI Interactions
    
    @IBAction func testWithNativeNetworkButtonTapped(_ sender: UIButton) {
        guard let getUrl = URL(string: sa3yApiUrlString_GET),
            let postUrl = URL(string: sa3yApiUrlString_POST),
            let openWeatherURL = urlComponents.url else {
            print("Unable to init one of the URL's for some reason. Just make sure its a valid URL and try again")
            return
        }
        
        //MARK: Open Weather Task
        let openWeatherUrlTask = urlSession.dataTask(with: openWeatherURL) { (data, urlResponse, error) in
            
            self.endTime = Date()
            
            self.printTimeElapsed(self.startTime, self.endTime)
            
            if error != nil {
                debugPrint(error!)
            }
            else {
                print(data)
            }
        }
        
        startTime = Date()
        openWeatherUrlTask.resume()
    }
    
    @IBAction func testWithAlamoFireNetworkingButtonTapped(_ sender: UIButton) {
        startTime = Date()
        AF.request(urlComponents.string!, method: .get)
            .responseJSON { (responseData) in
                self.endTime = Date()
                self.printTimeElapsed(self.startTime, self.endTime)
                if let error = responseData.error {
                    debugPrint(error)
                }
                else {
                    print(responseData.value)
                }
        }
    }
    
    private func printTimeElapsed(_ startTime:Date?, _ endTime: Date?) {
        if startTime != nil, endTime != nil {
            let timeElapsed = DateInterval(start: startTime!, end: endTime!)
            print("Time elapsed: \(timeElapsed.duration)")
        }
    }
}

